#before runnning the codes below, installl the object detection API from the link below
#https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2.md

import tensorflow as tf
#print(tf.__version__)
import cv2
from matplotlib import pyplot as plt
plt.style.use('dark_background')
import numpy as np
import imutils
import easyocr
from PIL import Image
import re
import os
import tarfile
import urllib.request
from object_detection.utils import label_map_util
from object_detection.utils import config_util
from object_detection.utils import visualization_utils as viz_utils
from object_detection.builders import model_builder
import PIL
#!pip uninstall PyQt5
#!pip install PyQt5==5.12.0 --user
#import pytesseract

!pip install easyocr

#Vehicle extraction using object detection using models from tensorflow object detection zoo

DATA_DIR = os.path.join(os.getcwd(), 'data')
MODELS_DIR = os.path.join(DATA_DIR, 'models')
for dir in [DATA_DIR, MODELS_DIR]:
    if not os.path.exists(dir):
        os.mkdir(dir)

# Download and extract model
MODEL_DATE = '20200713'
MODEL_NAME = 'centernet_hg104_512x512_coco17_tpu-8'
MODEL_TAR_FILENAME = MODEL_NAME + '.tar.gz'
MODELS_DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/tf2/'
#the download link can be used directly
MODEL_DOWNLOAD_LINK = MODELS_DOWNLOAD_BASE + MODEL_DATE + '/' + MODEL_TAR_FILENAME
PATH_TO_MODEL_TAR = os.path.join(MODELS_DIR, MODEL_TAR_FILENAME)
PATH_TO_CKPT = os.path.join(MODELS_DIR, os.path.join(MODEL_NAME, 'checkpoint/'))
PATH_TO_CFG = os.path.join(MODELS_DIR, os.path.join(MODEL_NAME, 'pipeline.config'))
if not os.path.exists(PATH_TO_CKPT):
    print('Downloading model. This may take a while... ', end='')
    urllib.request.urlretrieve(MODEL_DOWNLOAD_LINK, PATH_TO_MODEL_TAR)
    tar_file = tarfile.open(PATH_TO_MODEL_TAR)
    tar_file.extractall(MODELS_DIR)
    tar_file.close()
    os.remove(PATH_TO_MODEL_TAR)
    print('Done')

    
# Download labels file
LABEL_FILENAME = 'mscoco_label_map.pbtxt'
LABELS_DOWNLOAD_BASE = \
    'https://raw.githubusercontent.com/tensorflow/models/master/research/object_detection/data/'
PATH_TO_LABELS = os.path.join(MODELS_DIR, os.path.join(MODEL_NAME, LABEL_FILENAME))
if not os.path.exists(PATH_TO_LABELS):
    print('Downloading label file... ', end='')
    urllib.request.urlretrieve(LABELS_DOWNLOAD_BASE + LABEL_FILENAME, PATH_TO_LABELS)
    print('Done')

    
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Suppress TensorFlow logging
tf.get_logger().setLevel('ERROR')           # Suppress TensorFlow logging (2)



# Load pipeline config and build a detection model
configs = config_util.get_configs_from_pipeline_file(PATH_TO_CFG)
model_config = configs['model']
detection_model = model_builder.build(model_config=model_config, is_training=False)

# Restore checkpoint
ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)
ckpt.restore(os.path.join(PATH_TO_CKPT, 'ckpt-0')).expect_partial()

@tf.function
def detect_fn(image):
    """Detect objects in image."""

    image, shapes = detection_model.preprocess(image)
    prediction_dict = detection_model.predict(image, shapes)
    detections = detection_model.postprocess(prediction_dict, shapes)

    return detections, prediction_dict, tf.reshape(shapes, [-1])
    
#load image 
path='15.jpg'
img = cv2.imread(path)
#plt.figure(figsize=(50, 10))
#plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


image_np = cv2.imread(path)

input_tensor = tf.convert_to_tensor(np.expand_dims(image_np, 0), dtype=tf.float32)
detections, predictions_dict, shapes = detect_fn(input_tensor)

label_id_offset = 1
image_np_with_detections = image_np.copy()

#Vehicle detection using Object detection API 
im3=viz_utils.visualize_boxes_and_labels_on_image_array(
      image_np_with_detections,
      detections['detection_boxes'][0].numpy(),
      (detections['detection_classes'][0].numpy() + label_id_offset).astype(int),
      detections['detection_scores'][0].numpy(),
      category_index=3,
    #specific to cars only
      use_normalized_coordinates=True,
      max_boxes_to_draw=200,
      min_score_thresh=.50,
      line_thickness=1,
      agnostic_mode=True)
A=image_np_with_detections

area=[]
area.append(list(detections['detection_boxes'][0].numpy()[0]))
# top left  bottom right

image = PIL.Image.open(path)
width, height = image.size

img=cv2.imread(path)
for im in area:
    top=int(height*im[0]*1.2)
    left=int(width*im[1]*1.2)
    bottom=int(height*im[2])
    right=int(width*im[3]*0.8)
    img = img[top:bottom, left:right]
    #plt.figure(figsize=(50, 10))
    #plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))

img_ori = img
height, width, channel = img_ori.shape
gray = cv2.cvtColor(img_ori, cv2.COLOR_BGR2GRAY)

## Apply structuring (making initial format for further filter application) and apply TOPHAT and BLACKHAT morph

structuringElement = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

imgTopHat = cv2.morphologyEx(gray, cv2.MORPH_TOPHAT, structuringElement)
imgBlackHat = cv2.morphologyEx(gray, cv2.MORPH_BLACKHAT, structuringElement)

imgGrayscalePlusTopHat = cv2.add(gray, imgTopHat)
gray = cv2.subtract(imgGrayscalePlusTopHat, imgBlackHat)

#plt.figure(figsize=(12, 10))
#plt.imshow(gray, cmap='gray')
#plt.axis('off')
#plt.show()

## Normal thresholding (adaptive) applied on the preprocessed image

img_blurred = cv2.GaussianBlur(gray, ksize=(5, 5), sigmaX=0)

img_thresh = cv2.adaptiveThreshold(
    img_blurred, 
    maxValue=255.0, 
    adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C, 
    thresholdType=cv2.THRESH_BINARY_INV, 
    blockSize=19, 
    C=9
)
contours, _= cv2.findContours(
    img_thresh, 
    mode=cv2.RETR_LIST, 
    #retrieves all of the contours without establishing any hierarchical relationships
    method=cv2.CHAIN_APPROX_SIMPLE   
)
temp_result = np.zeros((height, width, channel), dtype=np.uint8)
temp_result=cv2.drawContours(temp_result, contours=contours, contourIdx=-1, color=(255, 255, 255))

## Drawing all the obtained contours
temp_result = np.zeros((height, width, channel), dtype=np.uint8)

contours_dict = []

for contour in contours:
    x, y, w, h = cv2.boundingRect(contour)
    cv2.rectangle(temp_result, pt1=(x, y), pt2=(x+w, y+h), color=(255, 255, 255), thickness=2)
    
    # insert to dict
    contours_dict.append({
        'contour': contour,
        'x': x,
        'y': y,
        'w': w,
        'h': h,
        'cx': x + (w / 2),
        'cy': y + (h / 2)
    })

#plt.figure(figsize=(12, 10))
#plt.imshow(temp_result, cmap='gray')
#plt.axis('off')
#print(contours_dict)

## Mark possible contours on the basis of approximation of the area and length and breadth of the contours
MIN_AREA = 80
MIN_WIDTH, MIN_HEIGHT = 2, 8
MIN_RATIO, MAX_RATIO = 0.25, 1.0

possible_contours = []

cnt = 0
for d in contours_dict:
    area = d['w'] * d['h']
    ratio = d['w'] / d['h']
    
    if area > MIN_AREA \
    and d['w'] > MIN_WIDTH and d['h'] > MIN_HEIGHT \
    and MIN_RATIO < ratio < MAX_RATIO:
        d['idx'] = cnt
        cnt += 1
        possible_contours.append(d)
# visualize possible contours
temp_result = np.zeros((height, width, channel), dtype=np.uint8)

for d in possible_contours:
#     cv2.drawContours(temp_result, d['contour'], -1, (255, 255, 255))
    cv2.rectangle(temp_result, pt1=(d['x'], d['y']), pt2=(d['x']+d['w'], d['y']+d['h']), color=(255, 255, 255), thickness=2)

#plt.figure(figsize=(12, 10))
#plt.imshow(temp_result, cmap='gray')
#plt.axis('off')
#plt.savefig('Car-Boxes-byCharSize.png',bbox_inches = 'tight')
#plt.show()

## Forming groups on the basis of distance relations between them
MAX_DIAG_MULTIPLYER = 7 # 5
MAX_ANGLE_DIFF = 12.0 # 12.0
MAX_AREA_DIFF = 0.5 # 0.5
MAX_WIDTH_DIFF = 0.8
MAX_HEIGHT_DIFF = 0.2
MIN_N_MATCHED = 9 # 3

def find_chars(contour_list):
    matched_result_idx = []
    
    for d1 in contour_list:
        matched_contours_idx = []
        for d2 in contour_list:
            if d1['idx'] == d2['idx']:
                continue

            dx = abs(d1['cx'] - d2['cx'])
            dy = abs(d1['cy'] - d2['cy'])

            diagonal_length1 = np.sqrt(d1['w'] ** 2 + d1['h'] ** 2)
            #distance between centres
            distance = np.linalg.norm(np.array([d1['cx'], d1['cy']]) - np.array([d2['cx'], d2['cy']]))
            if dx == 0:
                angle_diff = 90
            else:
                #angle diff between centres only
                angle_diff = np.degrees(np.arctan(dy / dx))
            area_diff = abs(d1['w'] * d1['h'] - d2['w'] * d2['h']) / (d1['w'] * d1['h'])
            width_diff = abs(d1['w'] - d2['w']) / d1['w']
            height_diff = abs(d1['h'] - d2['h']) / d1['h']

            if distance < diagonal_length1 * MAX_DIAG_MULTIPLYER \
            and angle_diff < MAX_ANGLE_DIFF and area_diff < MAX_AREA_DIFF \
            and width_diff < MAX_WIDTH_DIFF and height_diff < MAX_HEIGHT_DIFF:
                matched_contours_idx.append(d2['idx'])

        # append this contour
        matched_contours_idx.append(d1['idx'])

        if len(matched_contours_idx) < MIN_N_MATCHED:
            continue

        matched_result_idx.append(matched_contours_idx)
        unmatched_contour_idx = []
        for d4 in contour_list:
            if d4['idx'] not in matched_contours_idx:
                unmatched_contour_idx.append(d4['idx'])

        unmatched_contour = np.take(possible_contours, unmatched_contour_idx)
        
        # the remaining ones are matched among themselves
        recursive_contour_list = find_chars(unmatched_contour)
        
        for idx in recursive_contour_list:
            matched_result_idx.append(idx)

        break

    return matched_result_idx
    
result_idx = find_chars(possible_contours)

matched_result = []
for idx_list in result_idx:
    matched_result.append(np.take(possible_contours, idx_list))

# visualize possible contours
temp_result = np.zeros((height, width, channel), dtype=np.uint8)
for r in matched_result:
    for d in r:
        #cv2.drawContours(temp_result, d['contour'], -1, (255, 255, 255))
        cv2.rectangle(temp_result, pt1=(d['x'], d['y']), pt2=(d['x']+d['w'], d['y']+d['h']), color=(255, 255, 255), thickness=2)

#plt.figure(figsize=(12, 10))
#plt.imshow(temp_result, cmap='gray')
#plt.axis('off')
#plt.savefig('Car-Boxes-byContourArrangement.png',bbox_inches = 'tight')
#plt.show()

## Visualizing the obtained contours on the original image
result_idx = find_chars(possible_contours)

matched_result = []
for idx_list in result_idx:
    matched_result.append(np.take(possible_contours, idx_list))
temp_result = np.zeros((height, width, channel), dtype=np.uint8)

for r in matched_result:
    for d in r:
        #cv2.drawContours(temp_result, d['contour'], -1, (255, 255, 255))
        cv2.rectangle(img_ori, pt1=(d['x'], d['y']), pt2=(d['x']+d['w'], d['y']+d['h']), color=(0, 0, 255), thickness=2)

#plt.figure(figsize=(12, 10))
#plt.imshow(img_ori, cmap='gray')
#plt.axis('off')
#plt.savefig('Car-OverlappingBoxes.png',bbox_inches = 'tight')
#plt.show()

## Obtaining the number plate from the contour list obtained from the previous cell, projecting the number plate
PLATE_WIDTH_PADDING = 1.0 # 1.3
PLATE_HEIGHT_PADDING = 1.2 # 1.5
MIN_PLATE_RATIO = 3
MAX_PLATE_RATIO = 10

plate_imgs = []
plate_infos = []

for i, matched_chars in enumerate(matched_result):
    sorted_chars = sorted(matched_chars, key=lambda x: x['cx'])
    #getting plate distance relations
    plate_cx = (sorted_chars[0]['cx'] + sorted_chars[-1]['cx']) / 2
    plate_cy = (sorted_chars[0]['cy'] + sorted_chars[-1]['cy']) / 2
    
    plate_width = (sorted_chars[-1]['x'] + sorted_chars[-1]['w'] - sorted_chars[0]['x']) * PLATE_WIDTH_PADDING
    
    sum_height = 0
    for d in sorted_chars:
        sum_height += d['h']
    plate_height = int(sum_height / len(sorted_chars) * PLATE_HEIGHT_PADDING)
    
    triangle_height = sorted_chars[-1]['cy'] - sorted_chars[0]['cy']
    triangle_hypotenus = np.linalg.norm(
        np.array([sorted_chars[0]['cx'], sorted_chars[0]['cy']]) - 
        np.array([sorted_chars[-1]['cx'], sorted_chars[-1]['cy']])
    )
    #projecting the number plate straight
    angle = np.degrees(np.arcsin(triangle_height / triangle_hypotenus))
    
    rotation_matrix = cv2.getRotationMatrix2D(center=(plate_cx, plate_cy), angle=angle, scale=1.0)
    
    img_rotated = cv2.warpAffine(img_thresh, M=rotation_matrix, dsize=(width, height))
    
    img_cropped = cv2.getRectSubPix(
        img_rotated, 
        patchSize=(int(plate_width), int(plate_height)), 
        center=(int(plate_cx), int(plate_cy))
    )
    
    if img_cropped.shape[1] / img_cropped.shape[0] < MIN_PLATE_RATIO or img_cropped.shape[1] / img_cropped.shape[0] < MIN_PLATE_RATIO > MAX_PLATE_RATIO:
        continue
    
    plate_imgs.append(img_cropped)
    plate_infos.append({
        'x': int(plate_cx - plate_width / 2),
        'y': int(plate_cy - plate_height / 2),
        'w': int(plate_width),
        'h': int(plate_height)
    })
    #plotting all the indexes of contour groups got by the previous cell
    plt.subplot(len(matched_result), 1, i+1)
    plt.imshow(img_cropped, cmap='gray')
    plt.axis('off')
    plt.savefig('Carplate2.jpg',bbox_inches = 'tight')
    plt.show()
    
## Defining logic for the numbers detected from the number plate
def tonum(s):
    dic=dict()
    dic['A']=4
    dic['B']=8
    dic['E']=3
    dic['G']=6
    dic['H']=4
    dic['I']=1
    dic['J']=3
    dic['K']=4
    dic['L']=4
    dic['N']=11
    dic['O']=0
    dic['P']=9
    dic['Q']=0
    dic['R']=2
    dic['S']=5
    dic['T']=7
    dic['U']=11
    dic['Z']=4
    s=[_ for _ in s]
    for i in range(len(s)):
        if s[i] in dic:
            s[i]=str(dic[s[i]])
    return ''.join(s)
# may add as per the requirements

#### Finally predicting the result (Is currently best suited for MH vehicles)
reader = easyocr.Reader(['en'])
result = reader.readtext(img_cropped)
l=[]
for i in result:
    for j in i:
        if type(j)==str:
            if len(l)!=0 or (j.startswith(('MH','mH','hh','Mh','KH','kH','NH','nH','Kh','HH','Hh','hH'))):
                l.append(i[-2])
                l.append(' ')
finalresult=''.join(l)
if finalresult:
    print('Before processing',finalresult)

printed=0
if len(l)==0:
    for i in result:
        for j in i:
            if type(j)==str:
                if j.startswith('MH''mH''Mh'):
                    print(j)
                    printed=1
if printed==0 and len(l)==0:
    for i in result:
        for j in i:
            if type(j)==str:

                l.append(i[-2])
                l.append(' ')
    ''.join(l)
    print('Printed when l is empty')
    finalresult=''.join(l)
   

characters =['/','|','""','"','.','-']
for char in characters:
    finalresult=finalresult.replace(char,'')
finalresult = finalresult.replace(' ','')

finalresult=[_ for _ in finalresult]

for i in range(1,5):
    i=-1*i
    if not finalresult[i].isnumeric():
        finalresult[i]=tonum(finalresult[i])
for i in range(len(finalresult)):
    finalresult[i]=finalresult[i].upper()
for i in range(7,9):
    i=-1*i
    if not finalresult[i].isnumeric():
        finalresult[i]=finalresult[i].upper()
        finalresult[i]=tonum(finalresult[i])


finalresult=''.join(finalresult)

print('After processing ', finalresult)